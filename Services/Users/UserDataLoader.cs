﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphQL.Api.Services.Users
{
    public class UserDataLoader : BatchDataLoader<string, UserType>
    {
        public const int MAX_FIREBASE_USERS_BATCH_SIZE = 100;
        private readonly FirebaseAuth _firebaseAuth;

        public UserDataLoader(
            FirebaseApp firebaseApp,
            IBatchScheduler batchScheduler) : base(batchScheduler, new DataLoaderOptions
            {
                MaxBatchSize = MAX_FIREBASE_USERS_BATCH_SIZE
            }) =>
            _firebaseAuth = FirebaseAuth.GetAuth(firebaseApp);

        protected override async Task<IReadOnlyDictionary<string, UserType>> LoadBatchAsync(
            IReadOnlyList<string> userIds, 
            CancellationToken cancellationToken)
        {
            //var userIdentifiers = _firebaseAuth.GetUsersAsync(userIds.Select(u => new UidIdentifier);

            List<UidIdentifier> userIdentifiers = userIds.Select(u => new UidIdentifier(u)).ToList();
            GetUsersResult usersResult = await _firebaseAuth.GetUsersAsync(userIdentifiers);
            return usersResult.Users.Select(u => new UserType()
            {
                Id = u.Uid,
                UserName = u.DisplayName,
                PhotoUrl = u.PhotoUrl
            }).ToDictionary(u => u.Id);
        }
    }
}
