﻿namespace GraphQL.Api.Services.Instructors
{
    public class InstructorsRepository
    {
        private readonly IDbContextFactory<SchoolDbContext> _contextFactory;

        public InstructorsRepository(IDbContextFactory<SchoolDbContext> contextFactory) => _contextFactory = contextFactory;

        public async Task<InstructorDto> GetById(Guid instructorId)
        {
            await using SchoolDbContext context = _contextFactory.CreateDbContext();
            return await context.Instructors
                .FirstOrDefaultAsync(c => c.Id == instructorId);
        }

        public async Task<IEnumerable<InstructorDto>> GetManyByIds(IReadOnlyList<Guid> instructorIds)
        {
            await using SchoolDbContext context = _contextFactory.CreateDbContext();
            return await context.Instructors
                .Where(i => instructorIds.Contains(i.Id)).ToListAsync();
        }
    }
}
