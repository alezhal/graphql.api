﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphQL.Api.Services.Instructors
{
    public class InstructorDataLoader : BatchDataLoader<Guid, InstructorDto>
    {
        private readonly InstructorsRepository _instructorsRepository;
        public InstructorDataLoader(IBatchScheduler batchScheduler, DataLoaderOptions? options = null, InstructorsRepository instructorsRepository = null) : base(batchScheduler, options)
        {
            _instructorsRepository=instructorsRepository;
        }

        protected override async Task<IReadOnlyDictionary<Guid, InstructorDto>> LoadBatchAsync(IReadOnlyList<Guid> keys, CancellationToken cancellationToken)
        {
            IEnumerable<InstructorDto> instructors = await _instructorsRepository.GetManyByIds(keys);
            return instructors.ToDictionary(i => i.Id);
        }
    }
}
