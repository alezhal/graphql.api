﻿namespace GraphQL.Api.Services
{
    public class SchoolDbContext : DbContext
    {
        public SchoolDbContext(DbContextOptions<SchoolDbContext> options)
            : base(options) { }

        public DbSet<CourseDto> Courses { get; set; } = null!;
        public DbSet<InstructorDto> Instructors { get; set; } = null!;
        public DbSet<StudentDto> Students { get; set; } = null!;
    }
}
