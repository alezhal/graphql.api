﻿using System.Linq;
namespace GraphQL.Api.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : class
    {
        private readonly IDbContextFactory<SchoolDbContext> _contextFactory;

        public EfRepository(IDbContextFactory<SchoolDbContext> contextFactory) => _contextFactory = contextFactory;

        public async Task<IEnumerable<T>> GetAll()
        {
            using SchoolDbContext context = _contextFactory.CreateDbContext();
            var entities = await context.Set<T>().ToListAsync();
            return entities;
        }

        public async Task<T> GetById(Guid id)
        {
            using SchoolDbContext context = _contextFactory.CreateDbContext();
            var entity = await context.Set<T>().FindAsync(id);
            return entity;
        }

        public async Task<T> Create(T entity)
        {
            using SchoolDbContext context = _contextFactory.CreateDbContext();
            await context.Set<T>().AddAsync(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public async Task<bool> Delete(Guid id)
        {
            using SchoolDbContext context = _contextFactory.CreateDbContext();
            var entity = context.Set<T>().Find(id);
            context.Set<T>().Remove(entity);
            return await context.SaveChangesAsync() > 0;
        }

        public async Task<T> Update(T entity)
        {
            using SchoolDbContext context = _contextFactory.CreateDbContext();
            context.Set<T>().Update(entity);
            await context.SaveChangesAsync();
            return entity;
        }
    }
}
