﻿namespace GraphQL.Api.Models
{
    public enum Subject
    {
        Mathematics,
        Science,
        History
    }
}
