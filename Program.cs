var builder = WebApplication.CreateBuilder(args);

var sqlConBuilder = new SqlConnectionStringBuilder
{
    ConnectionString = builder.Configuration.GetConnectionString("SchoolDbConnection"),
    UserID = builder.Configuration["SchoolUserId"],
    Password = builder.Configuration["SchoolPassword"]
};

builder.Services
    .AddPooledDbContextFactory<SchoolDbContext>(o => o.UseSqlServer(sqlConBuilder.ConnectionString).LogTo(Console.WriteLine));

builder.Services
    .AddGraphQLServer()
    .AddQueryType<Query>()
    .AddMutationType<CourseMutations>()
    .AddSubscriptionType<CourseSubscriptions>()
    .AddType<CourseType>()
    .AddType<InstructorType>()
    .AddTypeExtension<CourseQueries>()
    .AddFiltering()
    .AddSorting()
    .AddProjections()
    .AddAuthorization()
    .AddInMemorySubscriptions();

builder.Services.AddSingleton(FirebaseApp.Create());
builder.Services.AddFirebaseAuthentication();
builder.Services.AddAuthorization(
    o => o.AddPolicy("IsAdmin",
    p => p.RequireClaim(FirebaseUserClaimType.EMAIL, "zhalninreg3@me.com")));
//builder.Services.AddAuthentication()

builder.Services.AddScoped<CoursesRepository>();
builder.Services.AddScoped<InstructorsRepository>();
builder.Services.AddScoped<InstructorDataLoader>();
builder.Services.AddScoped<UserDataLoader>();
//builder.Services.AddScoped(typeof(IRepository<>),typeof(EfRepository<>));


var app = builder.Build();

app.UseRouting();
app.UseAuthentication();
//app.UseAuthorization();
app.UseWebSockets();
//app.MapGet("/", () => "Hello World!");
app.MapGraphQL();

app.Run();
