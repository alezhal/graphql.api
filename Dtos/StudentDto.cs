﻿namespace GraphQL.Api.Dtos
{
    public class StudentDto : PersonDto
    {
        public double GPA { get; set; }
        public IEnumerable<CourseDto> Courses { get; set; }
    }
}
