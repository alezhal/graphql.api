﻿namespace GraphQL.Api.Dtos
{
    public class InstructorDto : PersonDto
    {
        public double Salary { get; set; }
        public IEnumerable<CourseDto> Courses { get; set; }
    }
}
