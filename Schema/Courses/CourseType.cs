﻿namespace GraphQL.Api.Schema.Courses
{
    public class CourseType : ISearchResultType
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;

        public Subject Subject { get; set; }

        [IsProjected(true)]
        public Guid InstructorId { get; set; }

        [GraphQLNonNullType]
        public async Task<InstructorType> Instructor([Service] InstructorDataLoader instructorDataLoader)
        {
            InstructorDto instructorDto = await instructorDataLoader.LoadAsync(InstructorId, CancellationToken.None);
            return new InstructorType
            {
                Id = instructorDto.Id,
                FirstName = instructorDto.FirstName,
                LastName = instructorDto.LastName,
                Salary = instructorDto.Salary
            };
        }

        public IEnumerable<StudentType>? Students { get; set; }

        [IsProjected(true)]
        public string? CreatorId { get; set; }

        public async Task<UserType?> Creator([Service] UserDataLoader userDataLoader)
        {
            if (CreatorId is null)
                return null;
            return await userDataLoader.LoadAsync(CreatorId, CancellationToken.None);
        }
    }
}
