﻿namespace GraphQL.Api.Schema.Courses
{
    [ExtendObjectType(typeof(Query))]
    public class CourseQueries
    {
        private readonly CoursesRepository _coursesRepository;
        //private readonly IRepository<CourseDto> _coursesRepository;

        public CourseQueries(CoursesRepository coursesRepository) => _coursesRepository = coursesRepository;
        //public CourseQueries(IRepository<CourseDto> coursesRepository) => _coursesRepository = coursesRepository;

        public async Task<IEnumerable<CourseType>> GetCourses()
        {
            IEnumerable<CourseDto> courseDtos = await _coursesRepository.GetAll();
            return courseDtos.Select(c => new CourseType
            {
                Id = c.Id,
                Name = c.Name,
                Subject = c.Subject,
                InstructorId = c.InstructorId,
                CreatorId = c.CreatorId
            });
        }

        [UseDbContext(typeof(SchoolDbContext))]
        [UsePaging(IncludeTotalCount = true, DefaultPageSize = 10)]
        [UseProjection]
        [UseFiltering(typeof(CourseFilterType))]
        [UseSorting(typeof(CourseSortType))]
        public IQueryable<CourseType> GetPaginatedCourses([ScopedService] SchoolDbContext context)
        {
            return context.Courses.Select(c => new CourseType
            {
                Id = c.Id,
                Name = c.Name,
                Subject = c.Subject,
                InstructorId = c.InstructorId,
                CreatorId = c.CreatorId
            });
        }

        [UseOffsetPaging(IncludeTotalCount = true, DefaultPageSize = 10)]
        public async Task<IEnumerable<CourseType>> GetOffsetCourses()
        {
            IEnumerable<CourseDto> courseDtos = await _coursesRepository.GetAll();
            return courseDtos.Select(c => new CourseType
            {
                Id = c.Id,
                Name = c.Name,
                Subject = c.Subject,
                InstructorId = c.InstructorId,
                CreatorId = c.CreatorId
            });
        }

        public async Task<CourseType> GetCourseByIdAsync(Guid id)
        {
            CourseDto courseDto = await _coursesRepository.GetById(id);
            return new CourseType
            {
                Id = courseDto.Id,
                Name = courseDto.Name,
                Subject = courseDto.Subject,
                InstructorId = courseDto.InstructorId,
                CreatorId = courseDto.CreatorId
            };
        }
    }
}
