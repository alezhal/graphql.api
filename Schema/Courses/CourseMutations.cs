﻿using FirebaseAdminAuthentication.DependencyInjection.Models;
using System.Security.Claims;

namespace GraphQL.Api.Schema.Courses
{
    public class CourseMutations
    {
        private readonly CoursesRepository _coursesRepository;

        public CourseMutations(CoursesRepository coursesRepository) => _coursesRepository = coursesRepository;

        [Authorize]
        public async Task<CourseResult> CreateCourse(CourseInputType courseInput, [Service] ITopicEventSender  topicEventSender,
            ClaimsPrincipal claimsPrincipal)
        {
            string userId = claimsPrincipal.FindFirstValue(FirebaseUserClaimType.ID);
            
            CourseDto courseDto = new()
            {
                Name = courseInput.Name,
                Subject = courseInput.Subject,
                InstructorId = courseInput.InstructorId,
                CreatorId = userId
            };
            courseDto = await _coursesRepository.Create(courseDto);

            CourseResult course = new()
            {
                Id = courseDto.Id,
                Name = courseDto.Name,
                Subject = courseDto.Subject,
                InstructorId = courseDto.InstructorId
            };

            await topicEventSender.SendAsync(nameof(CourseSubscriptions.CourseCreated), course);
            return course;
        }

        [Authorize]
        public async Task<CourseResult> UpdateCourse(Guid id, CourseInputType courseInput,[Service] ITopicEventSender topicEventSender,
            ClaimsPrincipal claimsPrincipal)
        {
            string userId = claimsPrincipal.FindFirstValue(FirebaseUserClaimType.ID);

            CourseDto courseDto = await _coursesRepository.GetById(id);

            if(courseDto is null)
            {
                throw new GraphQLException(new Error("Course not found.", "COURSE_NOT_FOUND"));
            }

            if(courseDto.CreatorId != userId)
            {
                throw new GraphQLException(new Error("You do not have permission to update this course.", "INVALID_PERMISSION"));
            }

            courseDto.Name = courseInput.Name;
            courseDto.Subject = courseInput.Subject;
            courseDto.InstructorId = courseInput.InstructorId;
            courseDto = await _coursesRepository.Update(courseDto);

            CourseResult course = new()
            {
                Id = courseDto.Id,
                Name = courseDto.Name,
                Subject = courseDto.Subject,
                InstructorId = courseDto.InstructorId
            };

            string updateCourseTopic = $"{course.Id}_{nameof(CourseSubscriptions.CourseUpdated)}";
            await topicEventSender.SendAsync(updateCourseTopic, course);

            return course;
        }

        [Authorize(Policy = "IsAdmin")]
        public async Task<bool> DeleteCourse(Guid id)
        {
            try
            {
                return await _coursesRepository.Delete(id);
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
