﻿using HotChocolate.Execution;

namespace GraphQL.Api.Schema.Courses
{
    public class CourseSubscriptions
    {
        [Subscribe]
        public CourseResult CourseCreated([EventMessage] CourseResult course) => course;

        [SubscribeAndResolve]
        public ValueTask<ISourceStream<CourseResult>> CourseUpdated(Guid courseId, [Service] ITopicEventReceiver topicEventReceiver)
        {
            string updateCourseTopic = $"{courseId}_{nameof(CourseSubscriptions.CourseUpdated)}";
            return topicEventReceiver.SubscribeAsync<string, CourseResult>(updateCourseTopic);
        }
    }
}