﻿namespace GraphQL.Api.Schema
{
    public class UserType
    {
        public string Id { get; set; } = null!;

        public string UserName { get; set; } = null!;

        public string PhotoUrl { get; set; } = null!;
    }
}
