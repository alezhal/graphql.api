﻿namespace GraphQL.Api.Schema
{
    public class PersonType : ISearchResultType
    {
        public Guid Id { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }
    }
}
