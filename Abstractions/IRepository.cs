﻿namespace GraphQL.Api.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();

        Task<T> GetById(Guid id);

        Task<IEnumerable<T>> GetManyByIds(IReadOnlyList<Guid> ids);

        Task<T> Create(T entity);

        Task<T> Update(T course);

        Task<bool> Delete(Guid id);
    }
}
